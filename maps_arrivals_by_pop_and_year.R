#loads required libraries for mapping
library(jpplot2)
library(sf)

#loads muni lookup with muni ids to join with shapefile
muni.lookup <- read.csv("~/Documents/Tools/marple-datatypes/regions/se/municipalities.csv",
                        stringsAsFactors = F) %>%
  select(scb,
         label_short) %>%
  mutate(scb = ifelse(nchar(scb) == 4,
                      scb,
                      paste0("0", scb)))

#joins thematic data with muni lookup...
map.data <- left_join(arrivals_by_pop,
                      muni.lookup,
                      by = c("municipality" = "label_short"))

#...and shapefile
map.data <- left_join(jpplot2::se_municipality(),
                      map.data,
                      by = c("id" = "scb"))

#calculates quantiles of normalised arrivals data
quantile(map.data$arrivals_by_100k_pop, length.out = 6, na.rm = T)

#creates breaks
map.data <- map.data %>%
  mutate(brks = cut(arrivals_by_100k_pop,
                    breaks = c(0, 200, 400, 600, 800, 1000, 10000),
                    labels = c("<200", "200-400",  "400-600", "600-800", "800-1000", ">1000"),
                    include.lowest = T))

#draws plot
arrivals.map <- ggplot(data = map.data) +
  geom_polygon(mapping = aes(x = long,
                             y = lat,
                             group = group,
                             fill = brks),
               colour = "white",
               size = 0.1) +
  geom_polygon(data = jpplot2::se_county(),
               aes(x = long,
                   y = lat,
                   group = group),
               fill = NA,
               colour = "white",
               size = 0.5,
               inherit.aes = F) +
  scale_fill_manual(values = c('#f0f9e8','#ccebc5','#a8ddb5','#7bccc4','#43a2ca','#0868ac')) +
  theme_jpp_map() +
  facet_wrap(~date,
             ncol = 5) +
  coord_sf(crs = st_crs(3006),
           clip = "off") +
  guides(fill = guide_legend(nrow = 1)) +
  labs(title = "Så många har kommuner tagit emot",
       subtitle = "Antal mottagna nyanlända per 100 000 invånare, 2015-2019") +
  theme(axis.text = element_blank(),
        plot.subtitle = element_text(margin = margin(c(5, 0, 25, 0))))

arrivals.map

#saves plot
finalise_plot(arrivals.map,
              source_name = "Källa: Migrationsverket",
              "arrivals_map.png",
              height_pixels = 475)
